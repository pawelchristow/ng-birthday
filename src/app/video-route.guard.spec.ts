import { TestBed, async, inject } from '@angular/core/testing';

import { VideoRouteGuard } from './video-route.guard';

describe('VideoGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoRouteGuard]
    });
  });

  it('should ...', inject([VideoRouteGuard], (guard: VideoRouteGuard) => {
    expect(guard).toBeTruthy();
  }));
});
