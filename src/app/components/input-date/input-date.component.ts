import { Component, ElementRef, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss']
})
export class InputDateComponent implements OnInit {
  @Output() submitForm: EventEmitter<boolean> = new EventEmitter<boolean>();

  errorList = {
    notCompleteDate: 'Please provide a full Date',
    wrongDate: 'Don\'t lie!<br>😝<br>Type your real Date Of Birth',
  }
  inputDateForm = new FormGroup({
    day: new FormControl(''),
    month: new FormControl(''),
    year: new FormControl(''),
  })
  completeFields = {
    day: false,
    month: false,
    year: false,
  }
  formError: string;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.dayValueChange();
    this.monthValueChange();
    this.yearValueChange();
  }

  dayValueChange() {
    this.inputDateForm.controls.day.valueChanges.subscribe((val) => {
      this.formError = null;
      if(!val) {
        this.completeFields.day = false;
        return;
      }
      if(val > 3 || val.length === 2) {
        this.focusOnMonthField();
        this.completeFields.day = true;
      }
    });
  }

  monthValueChange() {
    this.inputDateForm.controls.month.valueChanges.subscribe((val) => {
      this.formError = null;
      if(!val) {
        this.completeFields.month = false;
        return;
      }
      if(val > 1 || val.length === 2) {
        this.focusOnYearField();
        this.completeFields.month = true;
      }
    });
  }

  yearValueChange() {
    this.inputDateForm.controls.year.valueChanges.subscribe((val) => {
      this.formError = null;
      if(!val) {
        this.completeFields.year = false;
        return;
      }
      if(String(val).length === 4) {
        this.completeFields.year = true;
        this.validateForm();
      }
    });
  }

  areAllFieldsComplete(): boolean {
    return this.completeFields.day && this.completeFields.month && this.completeFields.year;
  }

  validateForm() {
    if(!this.areAllFieldsComplete()) {
      this.formError = this.errorList.notCompleteDate;
      return;
    }

    const dayValue = this.inputDateForm.controls.day.value;
    const monthValue = this.inputDateForm.controls.month.value;
    const yearValue = this.inputDateForm.controls.year.value;
    if(Number(dayValue) !== 23 || Number(monthValue) !== 11 || Number(yearValue) !== 1990) {
      this.formError = this.errorList.wrongDate;
      return;
    }

    this.confirmValidation();
  }

  confirmValidation() {
    console.log('submitform');
    this.submitForm.emit(true);
  }

  keyDownHandler(event, inputName) {
    if (event.code === 'Backspace') {
      this.formError = null;
      if(!event.target.value) {
        if(inputName === 'month') {
          this.completeFields.month = false;
          this.focusOnDayField();
          return;
        }

        if(inputName === 'year') {
          this.completeFields.year = false;
          this.focusOnMonthField();
          return;
        }
      }
    }
  }

  resetForm() {
    this.inputDateForm.controls.day.setValue(null);
    this.inputDateForm.controls.month.setValue(null);
    this.inputDateForm.controls.year.setValue(null);
    this.formError = null;
  }

  private focusOnDayField() {
    const dayInput = this.el.nativeElement.querySelector('.form-control__day');
    dayInput.focus();
  }

  private focusOnMonthField() {
    const monthInput = this.el.nativeElement.querySelector('.form-control__month');
    monthInput.focus();
  }

  private focusOnYearField() {
    const yearInput = this.el.nativeElement.querySelector('.form-control__year');
    yearInput.focus();
  }
}
