export interface Video {
  name: string;
  url: string;
  id: string;
  order?: number;
}
