import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Video } from '../model/videos.interface';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDatabaseService {

  videos: Video[];

  constructor(private firebase: AngularFireDatabase) { }

  async getVideos(): Promise<Video[]> {
    if(!this.videos || this.videos.length === 0) {
      const videos = await this.getMoviesFromDb();
      this.videos = this.orderVideos(videos.val());
    }
    return Promise.resolve(this.videos);
  }

  getMoviesFromDb() {
    return this.firebase.list("/movies").query.once("value");
  }

  private orderVideos(videos: Video[]): Video[] {
    return videos.filter((video: Video) => video.id).sort((a, b) => a.order - b.order);
  }
}
