import { Component, OnInit } from '@angular/core';
import { Video } from '../../model/videos.interface';
import { FirebaseDatabaseService } from '../../services/firebase-database.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  loading = true;
  loadedVideos = 0;
  videos: Video[]

  constructor(public fireDatabaseService: FirebaseDatabaseService) { }

  ngOnInit() {
    this.fireDatabaseService.getVideos().then(videos => {
      this.videos = videos;
    })
  }

  isLoaded() {
    this.loadedVideos++;
    if(this.loadedVideos === this.videos.length) {
      this.loading = false;
    }
  }

}
