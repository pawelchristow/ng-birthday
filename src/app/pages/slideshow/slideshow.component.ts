import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.scss']
})
export class SlideshowComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  goToVideos() {
    this.router.navigateByUrl('/videos');
  }
}
