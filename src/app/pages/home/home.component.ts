import { Component, OnInit } from '@angular/core';
import { FirebaseDatabaseService } from '../../services/firebase-database.service';
import { Router } from '@angular/router';
import { SecurityService } from '../../services/security.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private firebaseService: FirebaseDatabaseService, private router: Router, private securityService: SecurityService) { }

  ngOnInit() {
  }

  submitForm(event) {
    if(event) {
      this.securityService.validDateOfBirth = true;
      this.router.navigateByUrl('/slideshow');
    }
  }

}
