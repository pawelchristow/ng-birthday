import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SlideshowComponent } from './pages/slideshow/slideshow.component';
import { VideosComponent } from './pages/videos/videos.component';
import { HomeComponent } from './pages/home/home.component';
import { VideoRouteGuard } from './video-route.guard';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'slideshow', component: SlideshowComponent, canActivate: [VideoRouteGuard] },
  { path: 'videos', component: VideosComponent, canActivate: [VideoRouteGuard] },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
