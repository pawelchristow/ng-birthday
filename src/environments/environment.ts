// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBXuIaIM0DJXAcVN2H54qRLtdILpWlWe9E",
    authDomain: "happy-birthday-c6f11.firebaseapp.com",
    databaseURL: "https://happy-birthday-c6f11.firebaseio.com",
    projectId: "happy-birthday-c6f11",
    storageBucket: "happy-birthday-c6f11.appspot.com",
    messagingSenderId: "342948067403",
    appId: "1:342948067403:web:7a8bfb0fa8e73def5a35ce",
    measurementId: "G-D0XJ8H246X"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
